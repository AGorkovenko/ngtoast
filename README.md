### Toast Notification Module
- [X] Create a reusable Angular Module that can be used as a shared module in any Angular application.
- [X] Create documentation for your module.

#### Requirements:
- [X] Notifications should be of 3 types: success, warning and error. With different colors representing different Notification.
- [X] A notification must have a heading, subheading and a message. Of which, subheading is optional.
- [X] Developer should be able to set a timeout for each notification. If not, a default of 5 seconds should be used.
- [X] Developer should be able to set a position for the notification. Positions can be top-left, top- right, bottom-left or bottom-right. If not set, default of top right should be used.
- [X] Developer should be able to set the maximum number of notifications visible at a time, if not set, default should be 5.
- [X] Rest of the notifications must be grouped. Grouped notifications should have a badge showing the number of notifications in the group.
- [X] Use Rxjs.

#### Bonus:
1. Use NgRx to manage state of notifications.

### Setup

**step 1:** add ToastrModule to app NgModule, with BrowserAnimationsModule
```typescript
...
import {ToastModule} from "./toast/toast.module";

@NgModule({
  imports: [
    ...
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot() // ToastrModule added
  ],
  providers: [...],
  bootstrap: [AppComponent]
})
...
```

**step 2:** use toast service in components
```typescript
...
import {ToastService} from "./toast/services/toast.service";

constructor(private toastService: ToastService) {}

ngOnInit(): void {
  this.toastService.showSuccess(                          // use different type of toast: .showSuccess(), .showInfo(), .showWarning() or .showError()
    "I using Toast modelu",                               // set title
    "My first practice for using this beautiful toast!",  // set message
    "by toast notification"                               // set subtitle - is optional
  );
}
...
```

**step 3:** options

| Option   | Type          | Default   | Description                  |
|----------|---------------|-----------|------------------------------|
| position | ToastPosition | TOP_RIGHT | Position on screen           |
| duration | number        | 2000      | Time to live in milliseconds |
| visible  | number        | 5         | Limit for showing toast      |

```typescript
  ...
  imports: [
    ...
    ToastModule.forRoot({
      position: ToastPosition.TOP_RIGHT, // TOP_RIGHT | TOP_LEFT | BOTTOM_RIGHT | BOTTOM_LEFT
      visible: 4,
      duration: 10000
    })
  ],
...
```

### Structure of Toast Module:

#### Angular
```
Toast [module]
|
|- Toast [component]
|
|- Toast Notification [component]
|
|- Toast Notification Panel [component]
```

#### Html with classes
```

<toast-notifications>
  |
  |    <toast-notification>
  |    |
  |    |    <toast-notification-panel>
  |    |    |
  |    |    |    <toast-notification-panel__icon></toast-notification-panel__icon>
  |    |    |
  |    |    |    <toast-notification-panel__content>
  |    |    |    |
  |    |    |    |    <toast-notification-panel__title></toast-notification-panel__title>
  |    |    |    |
  |    |    |    |    <toast-notification-panel__subtitle></toast-notification-panel__subtitle>
  |    |    |    |
  |    |    |    |    <toast-notification-panel__message></toast-notification-panel__message>
  |    |    |    |
  |    |    |    </toast-notification-panel__content>
  |    |    |
  |    |    </toast-notification-panel>
  |    |
  |    |    <toast-rest>
  |    |    |
  |    |    |    <toast-rest__count></toast-rest__count>
  |    |    |
  |    |    </toast-rest>
  |    |
  |    </toast-notification>
  |
</toast-notifications>
```

### TODO:
- [ ] make an opportunity set pause when hover on toast

- [ ] set position for each toast notification panel in realtime

- [ ] set count of notifications in realtime

- [ ] manage states of toast with types and created time

- [ ] make possibility set message like html structure

## Olympiad task 1

The application adds a solution to the olympiad problem for finding a natural number.
  
To run the application:

- `npm i`
- `npm start`
- click in navbar to **Natural Number 1**

![Screen of application Discovergy](screen-2.png)