import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {TestTaskComponent} from "./test-task/test-task.component";

const appRoutes: Routes = [
  {
    path: "",
    component: TestTaskComponent
  },
  {
    path: "nn",
    loadChildren: () => import("../app/natural-number/natural-number.module").then(m => m.NaturalNumberModule),
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {
}
