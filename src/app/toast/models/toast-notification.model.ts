import {ToastNotificationType} from "./toast-notification-type.enum";

export class ToastNotificationContent {
  title: string;
  message: string;
  subtitle?: string;

  constructor(title: string, message: string, subtitle?: string) {
    this.title = title;
    this.message = message;
    if (subtitle) this.subtitle = subtitle;
  }
}

export class ToastNotification {
  type: ToastNotificationType;
  content: ToastNotificationContent
}
