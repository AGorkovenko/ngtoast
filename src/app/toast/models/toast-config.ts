import {ToastPosition} from "./toast-position.enum";

export class ToastConfig {
  /**
   * Position toast notifications on screen
   * default: TOP_RIGHT
   */
  position?: ToastPosition;

  /**
   * How long toast notification will be displayed
   * default: 2000
   */
  duration?: number;

  /**
   * Maximum notifications showed without group
   * default: 5
   */
  visible?: number;

  constructor() {
    this.position = ToastPosition.TOP_RIGHT;
    this.duration = 2000;
    this.visible = 5;
  }
}
