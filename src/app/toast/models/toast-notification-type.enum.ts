export enum ToastNotificationType {
  UNKNOWN = "UNKNOWN",
  SUCCESS = "SUCCESS",
  INFO    = "INFO",
  WARNING = "WARNING",
  ERROR   = "ERROR"
}
