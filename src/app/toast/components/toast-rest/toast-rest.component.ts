import {ChangeDetectionStrategy, Component, OnInit} from "@angular/core";
import {Observable} from "rxjs";
import {ToastNotificationService} from "../../services/toast-notification.service";
import {ToastPosition} from "../../models/toast-position.enum";

@Component({
  selector: "toast-rest",
  template: `
    <ng-container *ngIf="count$ | async as count">
      <div class="toast-rest"
           [ngClass]="{'toast-rest--top-right': position === ToastPosition.TOP_RIGHT,
                       'toast-rest--top-left': position === ToastPosition.TOP_LEFT,
                       'toast-rest--bottom-right': position === ToastPosition.BOTTOM_RIGHT,
                       'toast-rest--bottom-left': position === ToastPosition.BOTTOM_LEFT}">
        <div class="toast-rest__count">more +{{count}}</div>
      </div>
    </ng-container>
  `,
  styleUrls: ["./toast-rest.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ToastRestComponent implements OnInit {

  count$: Observable<number>;

  position: ToastPosition;
  ToastPosition = ToastPosition;

  constructor(private toastNotificationsService: ToastNotificationService) {
  }

  ngOnInit(): void {
    this.count$   = this.toastNotificationsService.countStackToastNotifications$;
    this.position = this.toastNotificationsService.config.position;
  }

}
