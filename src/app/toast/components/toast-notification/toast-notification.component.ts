import {ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, HostListener, Input, OnDestroy, OnInit} from "@angular/core";
import {Subject, timer} from "rxjs";
import {takeUntil, tap} from "rxjs/operators";
import {FADE_ANIMATION, FADE_ANIMATION_DURATION} from "../../animations";
import {ToastNotification} from "../../models/toast-notification.model";
import {ToastNotificationService} from "../../services/toast-notification.service";
import {ToastState} from "../../../store/reducers/toast.reducer";
import {Store} from "@ngrx/store";
import {toastHide, toastShow} from "../../../store/actions/toast.actions";

@Component({
  selector: "toast-notification",
  template: `
    <div class="toast-notification" [@fade]="animationFadeState">
      <toast-notification-panel [content]="toastNotification.content" [type]="toastNotification.type"></toast-notification-panel>
      <ng-content></ng-content>
    </div>
  `,
  styleUrls: ["./toast-notification.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [FADE_ANIMATION]
})
export class ToastNotificationComponent implements OnInit, OnDestroy {

  animationFadeState: string = "in";

  @Input() toastNotification: ToastNotification;
  @Input() rest: number;

  private isDestroyed$: Subject<void>           = new Subject();
  private toastTimerIsDestroyed$: Subject<void> = new Subject();

  constructor(private element: ElementRef,
              private toastNotificationsService: ToastNotificationService,
              private cdr: ChangeDetectorRef,
              private store: Store<ToastState>) {
  }

  ngOnInit(): void {
    this.startTimerToHideToast();
    this.store.dispatch(toastShow());
  }

  @HostListener("click") onMouseClick(): void {
    this.hideToast();
  }

  @HostListener("mouseenter") onMouseEnter(): void {
    this.stopTimerToHideToast();
  }

  @HostListener("mouseleave") onMouseLeave(): void {
    this.startTimerToHideToast();
  }

  startTimerToHideToast(): void {
    timer(this.toastNotificationsService.config.duration)
    .pipe(
      tap(() => this.hideToast()),
      takeUntil(this.toastTimerIsDestroyed$)
    )
    .subscribe();
  }

  stopTimerToHideToast(): void {
    this.toastTimerIsDestroyed$.next();
  }

  hideToast(): void {
    this.fadeOutToast();
    timer(FADE_ANIMATION_DURATION)
    .pipe(
      tap(() => {
        this.removeToastElement();
        this.store.dispatch(toastHide());
      }),
      takeUntil(this.isDestroyed$)
    )
    .subscribe();
  }

  fadeOutToast(): void {
    this.animationFadeState = "out";
    this.cdr.markForCheck();
  }

  removeToastElement(): void {
    this.element.nativeElement.remove();
    this.toastNotificationsService.hideToastNotification();
  }

  ngOnDestroy(): void {
    this.toastTimerIsDestroyed$.next();
    this.toastTimerIsDestroyed$.complete();
    this.isDestroyed$.next();
    this.isDestroyed$.complete();
  }
}
