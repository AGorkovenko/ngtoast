import {Injectable, Optional} from "@angular/core";
import {BehaviorSubject, Observable} from "rxjs";
import {ToastNotification, ToastNotificationContent} from "../models/toast-notification.model";
import {ToastNotificationType} from "../models/toast-notification-type.enum";
import {FADE_ANIMATION_DURATION} from "../animations";
import {ToastConfig} from "../models/toast-config";

@Injectable()
export class ToastNotificationService {

  config: ToastConfig = new ToastConfig();

  private toastNotificationsSource: BehaviorSubject<ToastNotification[]> = new BehaviorSubject([]);
  toastNotifications$: Observable<ToastNotification[]>                   = this.toastNotificationsSource.asObservable();

  private stackToastNotifications: ToastNotification[]                = [];
  private countStackToastNotificationsSource: BehaviorSubject<number> = new BehaviorSubject(0);
  countStackToastNotifications$: Observable<number>                   = this.countStackToastNotificationsSource.asObservable();

  constructor(@Optional() config: ToastConfig) {
    if (config) {
      Object.assign(this.config, config);
    }
  }

  showSuccess(title: string, message: string, subtitle?: string): void {
    this.addToStack({
      type: ToastNotificationType.SUCCESS,
      content: new ToastNotificationContent(title, message, subtitle)
    });
  }

  showInfo(title: string, message: string, subtitle?: string): void {
    this.addToStack({
      type: ToastNotificationType.INFO,
      content: new ToastNotificationContent(title, message, subtitle)
    });
  }

  showWarning(title: string, message: string, subtitle?: string): void {
    this.addToStack({
      type: ToastNotificationType.WARNING,
      content: new ToastNotificationContent(title, message, subtitle)
    });
  }

  showError(title: string, message: string, subtitle?: string): void {
    this.addToStack({
      type: ToastNotificationType.ERROR,
      content: new ToastNotificationContent(title, message, subtitle)
    });
  }

  hideToastNotification(): void {
    const toastNotifications = this.toastNotificationsSource.getValue();
    toastNotifications.shift();
    this.toastNotificationsSource.next(toastNotifications);
    setTimeout(() => this.updateToastNotifications(), FADE_ANIMATION_DURATION / 2);
  }

  private addToStack(toastNotificationData: ToastNotification) {
    this.stackToastNotifications.push(toastNotificationData);
    this.updateToastNotifications();
  }

  private updateToastNotifications(): void {
    const toastNotifications = this.toastNotificationsSource.getValue();
    if (toastNotifications.length < this.config.visible) {
      const notification = this.stackToastNotifications.shift();
      if (notification) {
        toastNotifications.push(notification);
      }
    }
    this.countStackToastNotificationsSource.next(this.stackToastNotifications.length);
    this.toastNotificationsSource.next(toastNotifications);
  }

}
