import {ApplicationRef, ComponentFactoryResolver, EmbeddedViewRef, Inject, Injectable, Injector, Optional, PLATFORM_ID} from "@angular/core";
import {DOCUMENT, isPlatformBrowser} from "@angular/common";
import {ToastNotificationService} from "./toast-notification.service";
import {ToastComponent} from "../toast.component";
import {ToastConfig} from "../models/toast-config";
import {select, Store} from "@ngrx/store";
import {selectToastNotificationCount} from "../../store/selectors/toast.selectors";
import {ToastState} from "../../store/reducers/toast.reducer";

@Injectable({
  providedIn: "root"
})
export class ToastService {

  toastNotificationState$ = this.store.pipe(select(selectToastNotificationCount));

  constructor(@Optional() config: ToastConfig,
              private componentFactoryResolver: ComponentFactoryResolver,
              private appRef: ApplicationRef,
              @Inject(DOCUMENT) private document,
              @Inject(PLATFORM_ID) private platformId: Object,
              private injector: Injector,
              private toastNotificationsService: ToastNotificationService,
              private store: Store<ToastState>) {
    if (isPlatformBrowser(this.platformId)) {
      this.appendToastNotification();
    }
  }

  appendToastNotification(): void {
    const componentFactory         = this.componentFactoryResolver.resolveComponentFactory(ToastComponent);
    const componentRef             = componentFactory.create(this.injector);
    componentRef.instance.position = this.toastNotificationsService.config.position;

    const domElem = (componentRef.hostView as EmbeddedViewRef<any>).rootNodes[0] as HTMLElement;
    this.appRef.attachView(componentRef.hostView);
    this.document.body.appendChild(domElem);
  }

  showSuccess(title: string, message: string, subtitle?: string): void {
    if (isPlatformBrowser(this.platformId)) {
      this.toastNotificationsService.showSuccess(title, message, subtitle);
    }
  }

  showInfo(title: string, message: string, subtitle?: string): void {
    if (isPlatformBrowser(this.platformId)) {
      this.toastNotificationsService.showInfo(title, message, subtitle);
    }
  }

  showWarning(title: string, message: string, subtitle?: string): void {
    if (isPlatformBrowser(this.platformId)) {
      this.toastNotificationsService.showWarning(title, message, subtitle);
    }
  }

  showError(title: string, message: string, subtitle?: string): void {
    if (isPlatformBrowser(this.platformId)) {
      this.toastNotificationsService.showError(title, message, subtitle);
    }
  }
}
