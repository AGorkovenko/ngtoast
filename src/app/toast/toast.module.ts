import {ModuleWithProviders, NgModule, Optional, SkipSelf} from "@angular/core";
import {CommonModule} from "@angular/common";
import {ToastComponent} from "./toast.component";
import {ToastNotificationComponent} from "./components/toast-notification/toast-notification.component";
import {ToastNotificationPanelComponent} from "./components/toast-notification-panel/toast-notification-panel.component";
import {ToastService} from "./services/toast.service";
import {ToastNotificationService} from "./services/toast-notification.service";
import {ToastConfig} from "./models/toast-config";
import {ToastRestComponent} from "./components/toast-rest/toast-rest.component";


@NgModule({
  declarations: [
    ToastComponent,
    ToastNotificationComponent,
    ToastNotificationPanelComponent,
    ToastRestComponent
  ],
  imports: [
    CommonModule
  ],
  providers: [
    ToastService,
    ToastNotificationService
  ],
  entryComponents: [
    ToastComponent
  ]
})
export class ToastModule {
  constructor(@Optional() @SkipSelf() toastModule: ToastModule) {
    if (toastModule) {
      throw new Error("ToastModule is already loaded. Import it in the AppModule only");
    }
  }

  static forRoot(config?: ToastConfig): ModuleWithProviders {
    return {
      ngModule: ToastModule,
      providers: [
        {provide: ToastConfig, useValue: config}
      ]
    };
  }
}
