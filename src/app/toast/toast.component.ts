import {ChangeDetectionStrategy, Component, OnInit} from "@angular/core";
import {Observable} from "rxjs";
import {ToastNotification} from "./models/toast-notification.model";
import {ToastNotificationService} from "./services/toast-notification.service";
import {ToastPosition} from "./models/toast-position.enum";

@Component({
  selector: "toast",
  template: `
    <ng-container *ngIf="toastNotifications$ | async as toastNotifications">
      <div class="toast-notifications"
           [ngClass]="{'toast-notifications--top-right': position === ToastPosition.TOP_RIGHT,
                       'toast-notifications--top-left': position === ToastPosition.TOP_LEFT,
                       'toast-notifications--bottom-right': position === ToastPosition.BOTTOM_RIGHT,
                       'toast-notifications--bottom-left': position === ToastPosition.BOTTOM_LEFT}">
        <ng-container *ngFor="let toastNotification of toastNotifications; last as isLast">
          <toast-notification [toastNotification]="toastNotification"></toast-notification>
          <toast-rest *ngIf="isLast"></toast-rest>
        </ng-container>
      </div>
    </ng-container>
  `,
  styleUrls: ["./toast.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ToastComponent implements OnInit {

  toastNotifications$: Observable<ToastNotification[]>;

  position: ToastPosition;
  ToastPosition = ToastPosition;

  constructor(private toastNotificationsService: ToastNotificationService) {
  }

  ngOnInit(): void {
    this.toastNotifications$ = this.toastNotificationsService.toastNotifications$;
  }

}
