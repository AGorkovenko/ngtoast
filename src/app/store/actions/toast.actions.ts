import {createAction, props} from "@ngrx/store";

export const toastShow = createAction("[Toast notification] show");
export const toastHide = createAction("[Toast notification] hide");
export const toastReset = createAction("[Toast notification] reset");
