import {Action, createReducer, on} from "@ngrx/store";
import * as ToastActions from "../actions/toast.actions";

export interface ToastState {
  countShow: number;
  countHide: number;
}

export const initialToastState: ToastState = {
  countShow: 0,
  countHide: 0
};

const toastStateReducer = createReducer(
  initialToastState,
  on(ToastActions.toastShow, state => ({...state, countShow: state.countShow + 1})),
  on(ToastActions.toastHide, state => ({...state, countHide: state.countHide + 1})),
  on(ToastActions.toastReset, state => ({countShow: 0, countHide: 0}))
);

export function reducer(state: ToastState | undefined, action: Action) {
  return toastStateReducer(state, action);
}
