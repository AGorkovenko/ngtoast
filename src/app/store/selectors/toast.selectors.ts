import {createSelector} from "@ngrx/store";
import {ToastState} from "../reducers/toast.reducer";

export const selectToastNotificationCount = createSelector(
  (state: ToastState) => state.countShow,
  (state: ToastState) => state.countHide,
  (state: number) => state
);
