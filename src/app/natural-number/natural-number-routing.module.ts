import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {TaskOneComponent} from "./components/task-one/task-one.component";
import {TaskTwoComponent} from "./components/task-two/task-two.component";

const naturalNumberRoutes: Routes = [
  {
    path: "",
    redirectTo: "1"
  },
  {
    path: "1",
    component: TaskOneComponent
  },
  {
    path: "2",
    component: TaskTwoComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(naturalNumberRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class NaturalNumberRoutingModule {
}
