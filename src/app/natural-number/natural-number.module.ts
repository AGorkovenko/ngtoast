import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {NaturalNamberComponent} from "./natural-namber.component";
import {NaturalNumberRoutingModule} from "./natural-number-routing.module";
import {TaskOneComponent} from "./components/task-one/task-one.component";
import {TaskTwoComponent} from "./components/task-two/task-two.component";
import {FormsModule} from "@angular/forms";
import {ToastService} from "../toast/services/toast.service";


@NgModule({
  declarations: [
    NaturalNamberComponent,
    TaskOneComponent,
    TaskTwoComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    NaturalNumberRoutingModule
  ],
  providers: [
    ToastService
  ]
})
export class NaturalNumberModule {
}
