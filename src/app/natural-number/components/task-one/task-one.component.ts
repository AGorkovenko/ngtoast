import {Component, OnInit} from "@angular/core";
import {ToastService} from "../../../toast/services/toast.service";

@Component({
  selector: "app-task-one",
  templateUrl: "./task-one.component.html",
  styleUrls: ["./task-one.component.scss"]
})
export class TaskOneComponent implements OnInit {

  numberN: number;
  numberP: number;

  limit = 2000000000000;

  constructor(private toastService: ToastService) {
  }

  ngOnInit() {
  }

  findNumberP() {
    this.numberP = -1;
    if (!this.numberN) {
      this.toastService.showError("Number N", "Invalid number entered");
    } else if (this.numberN < 0) {
      this.toastService.showError("Number N", "Number " + this.numberN + " is less than 0");
    } else if (this.numberN > this.limit) {
      this.toastService.showError("Number N", "Number " + this.numberN + " is more than 2000000000000");
    } else {
      this.numberP = this.recursiveFinding(this.numberN, 2);
    }
  }

  recursiveFinding(psevdoP: number, n: number) {
    // Get digits from number N
    let digits = this.getDigitsFromNumber(psevdoP);

    // Init array where will write all find number bigger than number N
    let minFoundNumber = this.limit + 1;
    let indexOfDigitFoundNumber: number;

    digits.forEach((d1: number, index1: number) => {
      digits.forEach((d2: number, index2: number) => {

        // searching digit from left which smaller than current
        if (index2 > index1 && d2 - d1 < 0) {
          // copy original array digits, because we can change it
          const digits2 = Object.assign([], digits);

          // making number from digits array
          digits2[index1] = [digits2[index2], digits2[index2] = digits2[index1]][0];

          // making number from digits array
          const foundNumber = this.makeNumberFromDigits(digits2);

          // check if found number is smaller than previous
          if (foundNumber < minFoundNumber) {
            // wrote this number
            minFoundNumber = foundNumber;
            // wrote index of digits from which we find this number
            indexOfDigitFoundNumber = index2;
          }
        }
      });
    });

    if (minFoundNumber === this.limit + 1) {
      return -1;
    }

    if (indexOfDigitFoundNumber > 0) {
      // Get digits from number which found
      digits = this.getDigitsFromNumber(minFoundNumber);

      // Get right part after index in found number
      let partOfDigits: number[] = [];
      for (let i = 0; i < indexOfDigitFoundNumber; i++) {
        partOfDigits.push(digits.shift());
      }

      // Sorting right part from smaller to bigger digits
      partOfDigits = this.sortDown(partOfDigits);

      // Make new number via concat
      const digitsOfNumberP = partOfDigits.concat(digits);
      return this.makeNumberFromDigits(digitsOfNumberP);
    } else {
      return minFoundNumber;
    }
  }

  /**
   * Sort from bigger to smaller
   */
  sortDown(numbers: number[]) {
    return numbers.sort((a: number, b: number) => b - a);
  }

  /*
   * Get all digits from  some number
   */
  getDigitsFromNumber(n: number) {
    const digits: number[] = [];
    while (n > 0) {
      digits.push(n % 10);
      n = Math.floor(n / 10);
    }
    return digits;
  }

  /*
   * Make number from array of digits
   */
  makeNumberFromDigits(digits: number[]) {
    let numberString = "";
    digits
      .reverse()
      .forEach((d: number) => {
        numberString += d;
      });
    return parseInt(numberString);
  }

}
