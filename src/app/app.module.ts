import {BrowserModule} from "@angular/platform-browser";
import {NgModule} from "@angular/core";

import {AppRoutingModule} from "./app-routing.module";
import {AppComponent} from "./app.component";
import {CommonModule} from "@angular/common";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";

import {ToastModule} from "./toast/toast.module";
import {ToastPosition} from "./toast/models/toast-position.enum";

import {StoreModule} from "@ngrx/store";

import * as FromToast from "./store/reducers/toast.reducer";
import {FormsModule} from "@angular/forms";

import {TestTaskComponent} from "./test-task/test-task.component";

@NgModule({
  declarations: [
    AppComponent,
    TestTaskComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    AppRoutingModule,
    FormsModule,
    BrowserAnimationsModule,

    ToastModule.forRoot({
      position: ToastPosition.TOP_RIGHT,
      visible: 4,
      duration: 10000
    }),

    StoreModule.forRoot({countShow: FromToast.reducer, countHide: FromToast.reducer}),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor() {
  }
}
