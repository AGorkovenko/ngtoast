import {Component, OnInit} from "@angular/core";
import {ToastService} from "../toast/services/toast.service";
import {tap} from "rxjs/operators";

@Component({
  selector: "app-test-task",
  templateUrl: "./test-task.component.html",
  styleUrls: ["./test-task.component.scss"]
})
export class TestTaskComponent implements OnInit {

  title: string;
  subtitle: string;
  message: string;

  toastNotificationState;

  constructor(private toastService: ToastService) {
  }

  ngOnInit(): void {
    this.toastService.toastNotificationState$
      .pipe(
        tap(state => {
          setTimeout(() => this.toastNotificationState = state, 0);
        })
      ).subscribe();
  }

  success() {
    this.toastService.showSuccess(this.title ? this.title : "Success", this.message ? this.message : "Message is important for toast notification in 2020 year.", this.subtitle ? this.subtitle : "Oh ... eee this is cooler subtitle for Success!!!");
  }

  info() {
    this.toastService.showInfo(this.title ? this.title : "Info", this.message ? this.message : "Message is important for toast notification in 2020 year.", this.subtitle ? this.subtitle : "Oh ... eee this is cooler subtitle for Info!!!");
  }

  warning() {
    this.toastService.showWarning(this.title ? this.title : "Warning", this.message ? this.message : "Message is important for toast notification in 2020 year.", this.subtitle ? this.subtitle : "Oh ... eee this is cooler subtitle for Warning!!!");
  }

  error() {
    this.toastService.showError(this.title ? this.title : "Error", this.message ? this.message : "Message is important for toast notification in 2020 year.", this.subtitle ? this.subtitle : "Oh ... eee this is cooler subtitle for Error!!!");
  }
}
